import React, { Component } from 'react';
import { connect } from 'react-redux';
import { addItem } from '../redux/actions';
import { bindActionCreators } from 'redux';
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

import List from './list';

class App extends Component
{

    constructor()
    {
        super();
        
        this.state = { input : '' };

        this.handleAddItem = this.handleAddItem.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
    }

    handleInputChange(e)
    {
        this.setState({ input : e.target.value});
    }

    handleAddItem()
    {
        this.props.addItem(this.state.input);
    }

    render()
    {
        return (
        <Container>
            <CssBaseline />
            <Box display="block" mt={1} mb={1}>
                <List />
            </Box>
            <Box display="block" mt={1} mb={1}>
                <TextField variant="outlined" fullWidth size="small" helperText="New Item" onChange={this.handleInputChange}></TextField>
            </Box>
            <Box>
                <Button variant="outlined" color="primary" onClick={this.handleAddItem}>Add</Button>
            </Box>
        </Container>
        );
    }
}

const mapStateToProps = (state) => {
    const counter = state.counter || 0;

    return { counter };
}

const mapDispatchToPros = dispatch => {
    return bindActionCreators({ addItem }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToPros)(App);