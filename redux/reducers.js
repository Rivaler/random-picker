import { ADD_ITEM } from './actions';
import { combineReducers } from 'redux'

function items(state = [], action)
{
    switch(action.type) {
        case ADD_ITEM :
            return [...state, action.payload];
    }

    return state;
}

export default combineReducers({ items });