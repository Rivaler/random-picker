export const INCREMENT = 'INCREMENT';
export const DECREMENT = 'DECREMENT';
export const ADD_ITEM = 'ADD_ITEM';

let nextItemId = 1;

export function addItem( description ) {
    return {
        type : ADD_ITEM,
        payload : {
            description : description,
            id : nextItemId++
        }
    };
}